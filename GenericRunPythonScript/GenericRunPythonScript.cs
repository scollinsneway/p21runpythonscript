﻿/*
Copyright 2018 Steven Collins

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using log4net.Config;
using Microsoft.Scripting.Hosting;
using P21.Extensions.BusinessRule;
using Rule = P21.Extensions.BusinessRule.Rule;

namespace GenericRunPythonScript
{
    public class GenericRunPythonScript : Rule
    {
        /// <summary>The SQL query to get the path that scripts are saved in</summary>
        public const string BasePathQuery = @"SELECT Value
                                              FROM PythonSettings
                                              WHERE Name = 'path'";

        /// <summary>The object that gets passed back to P21, indicating whether the rule succeeded or not</summary>
        public readonly RuleResult Result = new RuleResult();
        /// <summary>Handles SQL access</summary>
        public readonly ISqlRunner SqlRunner;
        /// <summary>Handles UI access</summary>
        public readonly ISimpleUiProvider SimpleUiProvider;
        /// <summary>Variables that should be added to the Python scope before execution</summary>
        public readonly Dictionary<string, object> AdditionalVariables;
        /// <summary>Variables that should be removed from the Python scope before execution</summary>
        public readonly IEnumerable<string> VariablesToClear;
        /// <summary>Handles logging</summary>
        public new readonly ILog Log;
        /// <summary>This is a field so that tests can override what time the scripts think it is</summary>
        public DateTime CurrentTime = DateTime.Now;

        /// <summary>Used to give Python scripts simple logging syntax</summary>
        private readonly LogWrapper _pythonLog = new LogWrapper();

        /// <summary>Lets you specify a script path without needing to query a DB, useful for local development</summary>
        private string _scriptBasePath = null;
        /// <summary>The Python script's scope after execution</summary>
        private ScriptScope _scope = null;
        /// <summary>The path of the Python script being executed</summary>
        private string _currentScriptPath = null;

        /// <summary>
        /// Whether the rule is running against a P21 form
        /// </summary>
        private bool? _isFormRule = null;
        private bool IsFormRule
        {
            get
            {
                if (_isFormRule == null)
                {
                    try
                    {
                        _isFormRule = Data.XMLDatastream != null;
                    }
                    catch
                    {
                        // Apparently P21 throws an exception if you try to access XMLDataStream for non-form rules
                        _isFormRule = false;
                    }
                }

                return (bool) _isFormRule;
            }
        }

        public GenericRunPythonScript()
        {
            SqlRunner = new SqlRunner();
            SimpleUiProvider = new SimpleUiProvider();
            Log = LogManager.GetLogger(typeof(GenericRunPythonScript));
            AdditionalVariables = null;
        }

        /// <summary>
        /// Useful for mocking external dependencies for tests
        /// </summary>
        public GenericRunPythonScript(ISqlRunner sqlRunner,
            ISimpleUiProvider simpleUiProvider, 
            Dictionary<string, object> additionalVariables = null, 
            IEnumerable<string> variablesToClear = null, 
            string scriptBasePath = null)
        {
            SqlRunner = sqlRunner ?? new SqlRunner();
            SimpleUiProvider = simpleUiProvider ?? new SimpleUiProvider();
            Log = null;
            _scriptBasePath = scriptBasePath;
            AdditionalVariables = additionalVariables;
            VariablesToClear = variablesToClear;
        }

        public override string GetName()
        {
            return "Generic Run Python Script";
        }

        public override string GetDescription()
        {
            return "Runs a Python script to perform the rule's logic.  The name of the script to run is specified by giving one of the fields an alias in this format: \"=ScriptName\"";
        }

        public override RuleResult Execute()
        {
            Result.Success = true;

            try
            {
                if (P21SqlConnection != null)
                {
                    SqlRunner.ExistingConnection = P21SqlConnection;
                }
                else
                {
                    SqlRunner.Server = Session.Server;
                    SqlRunner.Database = Session.Database;
                    SqlRunner.UseWindowsAuthentication = true;
                }

                // A dubious workaround to make Moq happy without having to get rid of VerifyAll
                var test = SqlRunner.Server;
                test = SqlRunner.Database;

                InitLogging();

                // Figure out which scripts we're supposed to run
                var scriptPaths = GetScriptPaths();

                if (scriptPaths == null || scriptPaths.Count == 0)
                {
                    FailRule("Unable to determine script name.  The name of the script to be executed must be specified in a field's alias in this format: \"=ScriptName\".");
                }
                else
                {
                    foreach (var scriptPath in scriptPaths)
                    {
                        _currentScriptPath = scriptPath;

                        ExecuteScript(scriptPath);

                        // Bail early if one of the rules hits an error
                        if (Result.Success == false)
                        {
                            return Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FailRule(ex.ToString());
            }

            EndLogging();

            return Result;
        }

        /// <summary>
        /// Executes a single Python script
        /// </summary>
        /// <param name="scriptPath">Full path to the script to execute (not just what comes in through the alias)</param>
        protected virtual void ExecuteScript(string scriptPath)
        {
            var variables = BuildVariableDict();
            // Store the original Python variable values so we know which ones the script actually modified
            var originalVariableValues = variables.ToDictionary(x => x.Key, x => x.Value);

            HandleDocString(scriptPath, variables);

            // Bail if we didn't get passed variables that we need
            if (!Result.Success)
            {
                return;
            }

            // Actually execute the Python code
            _scope = Python.ExecutePythonScript(scriptPath, variables, VariablesToClear, out var scriptError, ConfigureScope);

            if (scriptError != null)
            {
                // The Python code (probably) threw an exception, the rule failed
                FailRule(scriptError);
            }
            else if(!RuleState.MultiRow && !IsFormRule)
            {
                // Put the updated values back into P21
                // We don't have to do this for MultiRow rules because the script directly modifies the P21 objects in that case
                foreach (DataField field in Data.Fields)
                {
                    var varName = Python.GetVariableName(field);
                    var output = _scope.GetVariable<object>(varName).ToString();

                    // The script can also directly modify FieldValue - don't overwrite with the script variable if it's done that
                    if (output != field.FieldValue && field.FieldValue == (string)originalVariableValues[varName])
                    {
                        field.FieldValue = output;
                    }
                }

                // P21 will set the focus to the most recently edited field by default
                // We want to leave the focus wherever the user left it unless the rule explicitly wants to change it
                var focusField = (from DataField x in Data.Fields
                                  where x.SetFocus == "Y"
                                  select x).FirstOrDefault();

                if (focusField == null)
                {
                    var triggerField = (from DataField x in Data.Fields
                                        where x.TriggerColumn == "Y"
                                        select x).FirstOrDefault();

                    if (triggerField != null)
                    {
                        Data.SetFocus(triggerField.FieldName);
                    }
                }
                else
                {
                    Data.SetFocus(focusField.FieldName);
                }
            }
        }

        protected virtual void ConfigureScope(ScriptScope scope, ScriptEngine engine, bool initialSetup)
        {
            if (initialSetup)
            {
                //Add folders to the path list so it can find our custom stuff
                engine.Execute("sys.path.append('DLLs/')", scope);

                //Reference some general stuff
                engine.Execute("import clr", scope);
                engine.Execute("clr.AddReferenceToFileAndPath('GenericRunPythonScript.dll')", scope);
                engine.Execute("clr.AddReference('System.Windows.Forms')", scope);
                engine.Execute("clr.AddReference('System.Data')", scope);

                // Import the DataTable extension methods
                engine.Execute("import GenericRunPythonScript", scope);
                engine.Execute("clr.ImportExtensions(GenericRunPythonScript.DataTableExtensions)", scope);

                //Import some miscellaneous stuff for convenience
                engine.Execute("from System.Windows.Forms import MessageBox, MessageBoxButtons, MessageBoxIcon, DialogResult", scope);
                engine.Execute("from System.Data import ParameterDirection, DataSet, DataTable, DataRow, DataColumn", scope);
                engine.Execute("from System import DBNull", scope);
                engine.Execute("from GenericRunPythonScript import *", scope);
            }
        }

        /// <summary>
        /// Parses the "doc string" that's optionally at the top of each script.
        /// Currently only specifies required fields, but the idea was that it could also do other things
        /// </summary>
        /// <param name="scriptPath">Full path to the script to parse</param>
        /// <param name="variables">The variables being passed to the script</param>
        /// <example>"""Required: customer_id, global_user_id"""</example>
        protected virtual void HandleDocString(string scriptPath, Dictionary<string, object> variables)
        {
            var docString = GetDocString(scriptPath);

            foreach (var line in docString.Split("\n\r".ToCharArray()))
            {
                // A doc string line should contain a colon to separate the key from its value
                if (!line.Contains(":")) continue;

                var pieces = line.Split(':');
                var key = pieces[0].Trim();
                var value = pieces[1].Trim();

                switch (key.ToUpper())
                {
                    case "REQUIRED":
                        var missingFields = GetMissingFields(value.Split(',').Select(x => x.Trim()));
                        if (missingFields.Length > 0)
                        {
                            FailForMissingFields(missingFields);
                        }

                        break;
                }

                if (!Result.Success)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Checks whether all of the required fields were actually passed to the rule
        /// </summary>
        /// <param name="givenFields">The fields passed to the rule</param>
        /// <param name="requiredFields">The fields the rule requires</param>
        /// <param name="scriptPath">Full path to the script</param>
        protected virtual void HandleMissingFields(IEnumerable<string> givenFields, IEnumerable<string> requiredFields, string scriptPath)
        {
            var givenFieldArray = givenFields as string[] ?? givenFields.ToArray();
            var missingFields = requiredFields.Except(givenFieldArray).ToList();

            if (missingFields.Count > 0)
            {
                FailForMissingFields(missingFields);
            }
        }

        /// <summary>
        /// Extracts the doc string from a script
        /// </summary>
        /// <param name="scriptPath">Full path to the script</param>
        /// <returns>The full contents of the doc string, if any</returns>
        protected virtual string GetDocString(string scriptPath)
        {
            var docString = "";
            var tripleQuoteCount = 0;

            foreach (var line in System.IO.File.ReadLines(scriptPath))
            {
                docString += line.Replace("\"\"\"", "");
                tripleQuoteCount += line.CountString("\"\"\"");

                if (tripleQuoteCount >= 2 || tripleQuoteCount == 0)
                {
                    break;
                }
            }

            return docString;
        }

        /// <summary>
        /// Puts together all of the variables that will be passed to the Python script
        /// </summary>
        /// <returns>A dictionary of the variables to be passed to the Python script and their values</returns>
        protected virtual Dictionary<string, object> BuildVariableDict()
        {
            var variables = new Dictionary<string, object>();

            if (!IsFormRule)
            {
                if (RuleState.MultiRow)
                {
                    // For multi-row rules, we turn each column in each DataTable into a list of DataCell objects named after that column
                    foreach (DataTable table in Data.Set.Tables)
                    {
                        if (table.Rows.Count == 0)
                            continue;

                        foreach (DataColumn col in table.Columns)
                        {
                            var cells = new List<DataCell>();

                            foreach (DataRow row in table.Rows)
                            {
                                cells.Add(new DataCell(table, col, row, (int)row["rowID"]));
                            }

                            variables[Python.SanitizeVariableName(col.ColumnName)] = cells;
                        }
                    }
                }
                else
                {
                    // Single row rules get simple variables named after the fields passed
                    foreach (DataField field in Data.Fields)
                    {
                        variables[Python.GetVariableName(field)] = field.FieldValue;
                    }
                }
            }

            // Add some extra objects to make life easier
            variables["result"] = Result;
            variables["SQL"] = SqlRunner;
            variables["CurrentTime"] = CurrentTime;
            variables["P21"] = this;
            variables["Log"] = _pythonLog;
            variables["UI"] = SimpleUiProvider;

            if (AdditionalVariables != null)
            {
                foreach (var kvp in AdditionalVariables)
                {
                    variables[kvp.Key] = kvp.Value;
                }
            }

            AddAdditionalVariables(variables);

            return variables;
        }

        /// <summary>
        /// Allows derived classes to easily add custom variables
        /// </summary>
        /// <param name="variables"></param>
        protected virtual void AddAdditionalVariables(Dictionary<string, object> variables)
        {
        }

        /// <summary>
        /// Finds full paths for all of the scripts we're supposed to run
        /// </summary>
        /// <returns>A sorted list of script paths to execute</returns>
        protected virtual List<string> GetScriptPaths()
        {
            Dictionary<string, int> scriptPaths;

            if (IsFormRule)
            {
                scriptPaths = new Dictionary<string, int>();
                var doc = Data.XMLDatastream.Document;
                var reportPath = doc.Descendants("REPORTNAME").First().Value;
                var scriptName = Path.GetFileNameWithoutExtension(reportPath);
                var scriptPath = BuildScriptPathFromName(scriptName);

                scriptPaths[scriptPath] = 1;
            }
            else
            {
                if (RuleState.MultiRow)
                {
                    scriptPaths = ExtractPathsFromFieldNames(Data.Set.Tables.Cast<DataTable>()
                                                                .SelectMany(x => x.Columns.Cast<DataColumn>()
                                                                                          .Select(y => y.ColumnName)));
                }
                else
                {
                    scriptPaths = ExtractPathsFromFieldNames(Data.Fields.Cast<DataField>().Select(x => x.FieldAlias));

                    if (scriptPaths.Count == 0)
                    {
                        // See if we're running from a menu item
                        var menuTextField = Data.Fields.Cast<DataField>().FirstOrDefault(x => x.FieldName == "menu_text");
                        if (menuTextField != null)
                        {
                            // Use the menu item's name as a script name
                            scriptPaths = ExtractPathsFromFieldNames(new[] { "=" + menuTextField.FieldValue });
                        }
                    }
                }
            }

            return scriptPaths.OrderBy(x => x.Value)
                              .ThenBy(x => x.Key)
                              .Select(x => x.Key)
                              .ToList();
        }

        /// <summary>
        /// Finds paths for all of the passed script names
        /// </summary>
        /// <param name="fieldNames">The script names passed to us by P21</param>
        /// <returns>A dictionary of script paths and the ordering criteria given to us by the user</returns>
        protected virtual Dictionary<string, int> ExtractPathsFromFieldNames(IEnumerable<string> fieldNames)
        {
            var scriptPaths = new Dictionary<string, int>();

            foreach (var name in fieldNames)
            {
                // Look for a leading number to tell us the order the scripts should run in
                var match = Regex.Match(name, @"^(\d*)=(.*)");

                if (match.Success)
                {
                    var scriptName = match.Groups[2].Value;
                    var scriptPath = BuildScriptPathFromName(scriptName);

                    if (!int.TryParse(match.Groups[1].Value, out var scriptOrder))
                    {
                        scriptOrder = int.MaxValue;
                    }

                    scriptPaths[scriptPath] = scriptOrder;
                }
            }

            return scriptPaths;
        }

        /// <summary>
        /// Builds the full path to a script from its name
        /// </summary>
        /// <param name="scriptName">The name of the script, excluding file extension</param>
        /// <returns>The full path to a script</returns>
        protected virtual string BuildScriptPathFromName(string scriptName)
        {
            var basePath = GetBasePath();
            return Path.Combine(basePath, scriptName + ".py");
        }

        /// <summary>
        /// Returns the path to the folder that scripts are stored in
        /// </summary>
        /// <returns>The path to the folder that scripts are stored in</returns>
        protected virtual string GetBasePath()
        {
            return _scriptBasePath ?? (_scriptBasePath = SqlRunner.ExecuteSql(BasePathQuery).Rows[0][0].ToString());
        }

        /// <summary>
        /// Returns the P21 DataField object for the passed Python variable name
        /// Useful for easily accessing properties other than FieldValue
        /// </summary>
        /// <param name="name">A Python variable name for a P21 field</param>
        /// <returns>The P21 DataField object for the passed Python variable name</returns>
        public virtual DataField GetField(string name)
        {
            return Data.Fields.Cast<DataField>().FirstOrDefault(x => Python.GetVariableName(x) == name);
        }

        /// <summary>
        /// Returns the P21 DataField that triggered the rule
        /// Only usable with single row rules
        /// </summary>
        /// <returns>The P21 DataField that triggered the rule</returns>
        public virtual DataField GetTriggerField()
        {
            return Data.Fields.Cast<DataField>().FirstOrDefault(x => x.TriggerColumn == "Y");
        }

        /// <summary>
        /// Sets the rule's success to false with a message, then logs it
        /// </summary>
        /// <param name="message">The message to show the user and log to file</param>
        protected virtual void FailRule(string message)
        {
            Result.Message = message;
            Result.Success = false;
            Log?.Error(message);
        }

        /// <summary>
        /// Sets up the log4net logging
        /// </summary>
        protected virtual void InitLogging()
        {
            // These are used to name the files
            GlobalContext.Properties["User"] = Session.UserID;
            GlobalContext.Properties["Database"] = Session.Database;

            // Used to include the XML of a rule with every log message for easy error duplication
            GlobalContext.Properties["DataXml"] = Data.ToXml();

            // log4net.config should live in the same folder as RunPythonScript.dll
            var assemblyPath = System.Reflection.Assembly.GetAssembly(typeof(GenericRunPythonScript)).Location;
            var assemblyFolder = Path.GetDirectoryName(assemblyPath);
            var configPath = Path.Combine(assemblyFolder, "log4net.config");
            XmlConfigurator.ConfigureAndWatch(new FileInfo(configPath));

            _pythonLog.Log = Log;
        }

        /// <summary>
        /// This forces the logger to let go of its file lock - P21 doesn't seem to get rid of the reference very fast, if at all
        /// </summary>
        protected virtual void EndLogging()
        {
            Log?.Logger.Repository.Shutdown();
        }

        /// <summary>
        /// Tells P21 to update the fields in the passed order
        /// </summary>
        /// <param name="colNames">Names of fields, in the order they should be updated</param>
        public virtual void SetFieldUpdateOrder(IEnumerable colNames)
        {
            Data.SetFieldUpdateOrder((from object colName in colNames select colName.ToString()).ToList());
        }

        /// <summary>
        /// Gets the value stored in a Python variable.  Only valid after execution.
        /// </summary>
        /// <param name="variableName">The name of the variable to retrieve</param>
        /// <returns>The value stored in the Python variable</returns>
        public virtual object GetPythonVariableValue(string variableName)
        {
            if (_scope == null) throw new InvalidOperationException();

            return _scope.GetVariable<object>(variableName);
        }

        /// <summary>
        /// Lists the names of any fields missing from the passed collection of field names
        /// </summary>
        /// <param name="fieldNames">A collection of field names to look for</param>
        /// <returns>An array of field names not present in the passed list</returns>
        public virtual string[] GetMissingFields(IEnumerable fieldNames)
        {
            var variableNames = new HashSet<string>(BuildVariableDict().Keys);
            var parsedFieldNames = new HashSet<string>(fieldNames.Cast<string>());

            return parsedFieldNames.Except(variableNames).ToArray();
        }

        /// <summary>
        /// Sets the rule's result to false and displays a message about the passed fields being missing
        /// </summary>
        /// <param name="missingFieldNames">The names of the fields that are missing</param>
        public virtual void FailForMissingFields(IEnumerable missingFieldNames)
        {
            var parsedFieldNames = new HashSet<string>(missingFieldNames.Cast<string>());
            var scriptName = Path.GetFileNameWithoutExtension(_currentScriptPath);
            var message = $"{scriptName} is missing the following required field{(parsedFieldNames.Count > 1 ? "s" : "")}:\n";

            foreach (var field in parsedFieldNames)
            {
                message += $"{field}\n";
            }

            FailRule(message);
        }
    }
}
