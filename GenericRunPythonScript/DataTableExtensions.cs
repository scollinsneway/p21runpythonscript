﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using IronPython.Runtime;

namespace GenericRunPythonScript
{
    /// <summary>
    /// DataTable shorthand, useful for tests
    /// </summary>
    public static class DataTableExtensions
    {
        /// <summary>
        /// Adds the passed columns to the DataTable
        /// </summary>
        /// <param name="table">The DataTable to modify</param>
        /// <param name="columns">Dictionary of column names and their data types</param>
        public static void AddColumns(this DataTable table, Dictionary<string, Type> columns)
        {
            foreach (var col in columns)
            {
                table.Columns.Add(new DataColumn(col.Key, col.Value));
            }
        }

        /// <summary>
        /// Adds the passed columns to the DataTable
        /// </summary>
        /// <param name="table">The DataTable to modify</param>
        /// <param name="rawColumnNames">Names of each column</param>
        public static void AddColumns(this DataTable table, IEnumerable rawColumnNames)
        {
            var names = rawColumnNames.OfType<string>().ToList();

            foreach (var name in names)
            {
                table.Columns.Add(new DataColumn(name));
            }
        }

        /// <summary>
        /// Creates a row with the passed values
        /// Columns are assumed to be in the same order as defined
        /// </summary>
        /// <param name="table">The DataTable to modify</param>
        /// <param name="values">The values to assign to each column in the row</param>
        public static void AddRow(this DataTable table, IEnumerable<object> values)
        {
            var newRow = table.NewRow();
            var counter = 0;

            foreach (var value in values)
            {
                newRow[counter] = value;
                counter++;
            }

            table.Rows.Add(newRow);
        }

        /// <summary>
        /// Tests whether two data tables have equivalent rows and columns
        /// </summary>
        /// <param name="table">One of the DataTables to compare</param>
        /// <param name="otherTable">The other DataTable to compare</param>
        /// <returns>True if the tables are equivalent, false if not</returns>
        public static bool IsEquivalentTo(this DataTable table, DataTable otherTable)
        {
            if (table == otherTable)
            {
                return true;
            }

            if (table == null
                || otherTable == null
                || table.Columns.Count != otherTable.Columns.Count
                || table.Rows.Count != otherTable.Rows.Count)
            {
                return false;
            }

            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (table.Columns[i].ColumnName != otherTable.Columns[i].ColumnName)
                {
                    return false;
                }
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (!table.Rows[i][j].Equals(otherTable.Rows[i][j]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
