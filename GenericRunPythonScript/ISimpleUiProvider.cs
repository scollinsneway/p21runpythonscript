﻿using System.Windows.Forms;

namespace GenericRunPythonScript
{
    public interface ISimpleUiProvider
    {
        DialogResult Message(string message, string title = "", MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.Information);
    }
}
