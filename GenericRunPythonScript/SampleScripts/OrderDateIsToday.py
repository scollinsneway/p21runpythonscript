"""Required: order_date"""

# Makes sure the order date is today and gives the user an opportunity to fix other dates if order date was wrong
# Should be triggered from a field that the user must enter to create an order

dateFormat = '%m/%d/%Y %H:%M:%S'

parsedDate = datetime.strptime(order_date, dateFormat)

# Uncomment this for testing
#parsedDate = parsedDate - timedelta(days=-1)

if parsedDate.date() != CurrentTime.date():
    order_date = datetime.strftime(CurrentTime, dateFormat)

    UI.Message("Warning: Order date was not set to today.  Double check any other dates on the order before saving.", "Old Date", icon=MessageBoxIcon.Warning)
