﻿using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace GenericRunPythonScript
{
    public interface ISqlRunner
    {
        /// <summary>Name of the SQL Server instance to connect to</summary>
        string Server { get; set; }
        /// <summary>Name of the database to connect to</summary>
        string Database { get; set; }
        /// <summary>SQL Authentication username to use for the connection</summary>
        string Username { get; set; }
        /// <summary>SQL Authentication password to use for the connection</summary>
        string Password { get; set; }
        /// <summary>Whether the connection should use Windows or SQL authentication</summary>
        bool UseWindowsAuthentication { get; set; }
        SqlConnection ExistingConnection { get; set; }

        /// <summary>Full connection string for the connection</summary>
        string ConnectionString { get; }

        /// <summary>
        /// Executes a SQL query against the defined connection, optionally with parameters
        /// </summary>
        /// <param name="query">The query to execute</param>
        /// <param name="rawParameters">The parameters to pass to the query.  Expected to contain Parameter objects</param>
        /// <returns>A DataTable of the query's results</returns>
        DataTable ExecuteSql(string query, IEnumerable rawParameters = null);

        /// <summary>
        /// Executes one or more SQL queries in a single transaction
        /// </summary>
        /// <param name="rawQueries">The queries to execute.  Expected to contain Query objects</param>
        void ExecuteTransaction(IEnumerable rawQueries);

        /// <summary>
        /// Asks the P21 database for a new uid for a specific table
        /// </summary>
        /// <param name="tableName">The name of the table to get a uid for</param>
        /// <returns>A new uid for the passed table</returns>
        int GetTableUid(string tableName);
    }
}
